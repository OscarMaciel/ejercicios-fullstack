package guia2;

import java.util.Arrays;

public class Ejercicio6 {

    public static void main(String[] args) {

        boolean[] esPrimo = new boolean[201];
        Arrays.fill(esPrimo, true);

        for (int i = 2; i <= 200; i += 2) {
            esPrimo[i] = false;
        }

        for (int i = 3; i <= Math.sqrt(200); i += 2) {
            if (esPrimo[i]) {
                for (int j = i * i; j <= 200; j += i) {
                    esPrimo[j] = false;
                }
            }
        }

        System.out.println("Los numeros primos menores a 200 son:");
        for (int i = 2; i <= 200; i++) {
            if (esPrimo[i]) {
                System.out.print(i + " ");
            }
        }
        System.out.println();
        
    }
}