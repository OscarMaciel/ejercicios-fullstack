package guia1;

import java.util.Scanner;

public class Ejercicio5 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("Ingrese el radio del circulo: ");
        double radio = scanner.nextDouble();
        
        double area = Math.PI * Math.pow(radio, 2);
        double perimetro = 2 * Math.PI * radio;
        
        System.out.printf("Area del circulo: %.2f%nPerimetro del circulo: %.2f%n", area, perimetro);
        
    }
}