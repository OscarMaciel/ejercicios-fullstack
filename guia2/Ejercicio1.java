package guia2;

import java.util.Scanner;

public class Ejercicio1 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner (System.in);
    
        System.out.print("Ingresa un numero: ");
    
        int numero = scanner.nextInt();
    
        System.out.println("Tabla de multiplicar del numero " + numero + " :");
    
        for (int i=1; i<=10; i++) 
        System.out.println(numero + "*" + i + "=" + numero*i);
        
    } 
}