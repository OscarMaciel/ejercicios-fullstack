﻿package guia1;

import java.util.Scanner;

public class Ejercicio2 {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        
        System.out.println("¡Hola!, ¿Cuál es tu nombre?");
        
        String nombre = s.nextLine();
        
        System.out.println("Hola, " + nombre);
        
    }
}
