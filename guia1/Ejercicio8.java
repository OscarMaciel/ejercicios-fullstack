package guia1;

import java.util.Scanner;

public class Ejercicio8 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Ingrese la temperatura en grados Celsius: ");
        float celsius = scanner.nextFloat();

        float fahrenheit = (celsius * 9/5) + 32f;
        float kelvin = celsius + 273.15f;

        System.out.printf("Temperatura en grados Fahrenheit: %.1f%nTemperatura en grados Kelvin: %.1f%n", fahrenheit, kelvin);
        
    }
}





