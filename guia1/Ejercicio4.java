package guia1;

import java.util.Scanner;

public class Ejercicio4 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("Ingrese la estatura de la primera persona: ");
        float estatura1 = scanner.nextFloat();
        
        System.out.print("Ingrese la estatura de la segunda persona: ");
        float estatura2 = scanner.nextFloat();
        
        System.out.print("Ingrese la estatura de la tercera persona: ");
        float estatura3 = scanner.nextFloat();
        
        float estaturaPromedio = (estatura1 + estatura2 + estatura3) / 3;
        
        System.out.printf("La estatura promedio es: %.2f%n", estaturaPromedio);
        
    }
}
