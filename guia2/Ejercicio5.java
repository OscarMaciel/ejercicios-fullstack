package guia2;

import java.util.Scanner;

public class Ejercicio5 {

    public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);

    System.out.print("Ingrese un numero: ");
    int n = scanner.nextInt();

    int[] fib = new int[n];
    fib[0] = 0;
    fib[1] = 1;
    for (int i = 2; i < n; i++) {
      fib[i] = fib[i - 1] + fib[i - 2];
    }

    System.out.println("Los primeros " + n + " terminos de la sucesion de Fibonacci son: " );
    for (int i = 0; i < n; i++) {
    System.out.print(fib[i] + "  ") ;
    } 
    System.out.println();
    
  }
}