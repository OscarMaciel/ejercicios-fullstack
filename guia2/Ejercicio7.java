package guia2;

import java.util.Scanner;

public class Ejercicio7 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Por favor responda las siguientes preguntas solamente con un Si o un No.");

        System.out.println("�Es un animal herbivoro?");
        boolean esHerbivoro = scanner.nextLine().equalsIgnoreCase("si");

        System.out.println("�Es un animal mamifero?");
        boolean esMamifero = scanner.nextLine().equalsIgnoreCase("si");

        System.out.println("�Es un animal domestico?");
        boolean esDomestico = scanner.nextLine().equalsIgnoreCase("si");

        if (esHerbivoro && esMamifero && esDomestico) {
            System.out.println("El animal es un caballo.");
        } else if (esHerbivoro && esMamifero && !esDomestico) {
            System.out.println("El animal es un alce.");
        } else if (esHerbivoro && !esMamifero && esDomestico) {
            System.out.println("El animal es una tortuga.");
        } else if (esHerbivoro && !esMamifero && !esDomestico) {
            System.out.println("El animal es un caracol.");
        } else if (!esHerbivoro && esMamifero && esDomestico) {
            System.out.println("El animal es un gato.");
        } else if (!esHerbivoro && esMamifero && !esDomestico) {
            System.out.println("El animal es un leon.");
        } else if (!esHerbivoro && !esMamifero && esDomestico) {
            System.out.println("El animal es un piton.");
        } else if (!esHerbivoro && !esMamifero && !esDomestico) {
            System.out.println("El animal es un condor.");
            
        }    
     }
 }