package guia1;

import java.util.Scanner;

public class Ejercicio6 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Ingrese el precio del producto: ");
        float precio = scanner.nextFloat();

        System.out.print("Ingrese el porcentaje de descuento: ");
        float porcentajeDescuento = scanner.nextFloat();

        float importeDescontado = precio * (porcentajeDescuento / 100);
        float precioFinal = precio - importeDescontado;

        System.out.printf("Importe descontado: %.2f%nImporte a pagar: %.2f%n", importeDescontado, precioFinal);
        
    }
}