package guia2;

import java.util.Scanner;

public class Ejercicio2 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner (System.in);
    
        System.out.print("Ingresa una palabra: ");
    
        String palabra = scanner.nextLine();
        
        boolean esPalindromo = true;
        
        for (int i = 0; i < palabra.length() / 2; i++) {
            if (palabra.charAt(i) != palabra.charAt(palabra.length() - i - 1)) {
                esPalindromo = false;
                break;
            }
        }

        if (esPalindromo) {
            System.out.println("La palabra " + palabra + " es un palindromo.");
        } else {
            System.out.println("La palabra " + palabra + " no es un palindromo.");
            
        }
    }
}