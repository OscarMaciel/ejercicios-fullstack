package guia1;

import java.util.Scanner;

public class Ejercicio7 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Ingrese la edad 1: ");
        int edad1 = scanner.nextInt();

        System.out.print("Ingrese la edad 2: ");
        int edad2 = scanner.nextInt();

        System.out.println("Edades Iniciales: ");
        System.out.println("Edad 1: " + edad1 + "\nEdad 2: " + edad2);

        int temp = edad1;
        edad1 = edad2;
        edad2 = temp;

        System.out.println("Edades Intercambiadas: ");
        System.out.println("Edad 1: " + edad1 + "\nEdad 2: " + edad2);
        
    }
}