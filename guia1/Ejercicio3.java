﻿package guia1;

import java.util.Scanner;

public class Ejercicio3 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in); 
        
        System.out.print("Ingresa el primer número: ");        
        int a = scanner.nextInt();    
        
        System.out.print("Ingresa el segundo número: ");        
        int b = scanner.nextInt();     
        
        System.out.println(a + " + " + b + " = " + (a + b));
        System.out.println(a + " - " + b + " = " + (a - b));
        System.out.println(a + " * " + b + " = " + (a * b));                   
        System.out.printf(a + " / " + b + " = " + "%.2f%n", ((float) a  / (float) b));
        
    }
}